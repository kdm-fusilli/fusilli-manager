package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._


import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, Router }
import scala.util.Random

object Mockingbird {
  def props = Props[Mockingbird]
}


class Mockingbird(instance: Int, job: Job, followers: Seq[ActorRef]) extends Actor {
  var index = 0

  private val log = Logging.getLogger(context.system, this)

    val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case a: (SupervisorStop,ActorRef) => 
      a._2 ! SupervisedAck

      
    case el: Map[String, String] =>
      index = index + 1
      log.info("MOCKINGBIRD" + s"${instance}"+ "OF " + s"${index}" + " ELEMENT")
      //log.info("WICH IS " + s"${el}" +"\n\n\n\n")
      //logger ! (LogMessage(instance, JobMessage("MOCKINGBIRD RESULT: " + s"${el}", "", job.job_configuration_id)))

      for (follower <- followers){ follower ! el }
  }

}
