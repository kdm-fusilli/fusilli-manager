package com.kdmforce.fusillimanager.impl.discovery

import akka.actor.{Actor, ActorRef, PoisonPill, Props, Terminated}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging

import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Job, Pipeline}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, PipelineStart, PipelineEnd}
//import com.kdmforce.fusillimanager.impl.MessageSerializer.LogMessageDiscovery
import com.kdmforce.fusillimanager.impl.connector.{ConnectorIn, ConnectorOut}
import com.kdmforce.fusillimanager.impl.job._
import com.kdmforce.fusillimanager.api.DiscoveryEndpointSerializer.DiscoveryDoc

object TheOtherOneAfterDispatcher {
  def props = Props[TheOtherOneAfterDispatcher]
}

class TheOtherOneAfterDispatcher(discoveryId: Int) extends Actor {

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
    )

  override def receive ={
    case request : DiscoveryDoc =>
      val restTalker = context.system.actorOf(Props(classOf[RestTalker], discoveryId, request))
      val explorer = context.system.actorOf(Props(classOf[Explorer], discoveryId, request.repository, restTalker))

      val GoMessage = Map[String,String]("condition"->"GO!")

//      logger ! (LogMessageDiscovery(discoveryId, JobMessage("START", "START AUTODISCOVERY", 0)))
      explorer ! PipelineStart
      explorer ! GoMessage

    case Terminated(_) =>
//      logger ! (LogMessageDiscovery(discoveryId, JobMessage("END", "END AUTODISCOVERY", 0)))
      self ! PoisonPill
  } 



}


