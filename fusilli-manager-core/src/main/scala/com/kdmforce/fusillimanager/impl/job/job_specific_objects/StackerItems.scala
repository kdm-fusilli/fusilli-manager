package com.kdmforce.fusillimanager.impl.job.job_specific_objects

import play.api.libs.json.{Format, Json, JsValue,JsObject,JsArray}

object StackerItems {

	case class StackerConfig (group_me : List[StackerConfigCouple], sum_me : List[StackerConfigCouple] ) 

	object StackerConfig{ implicit val format: Format[StackerConfig] = Json.format[StackerConfig] }




	case class StackerConfigCouple ( key_name : String, out_name : String ) 

	object StackerConfigCouple{ implicit val format: Format[StackerConfigCouple] = Json.format[StackerConfigCouple] }


}