package com.kdmforce.fusillimanager.impl.connector

import akka.actor.{Actor, ActorRef, PoisonPill, Props, Stash,Kill}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import akka.pattern.pipe
import akka.stream._
import akka.stream.scaladsl.{Flow, Keep, Sink, Source, SourceQueue}
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo,Attribute}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, LogMessage, _}
import com.kdmforce.fusillimanager.impl.connector.connection._
import com.kdmforce.fusillimanager.impl.connector.data_file._
import scala.concurrent.duration._


import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ConnectorOut{

  def props = Props[ConnectorOut]

}

class ConnectorOut(instance:Int, repo: Repo, entity: Entity) extends Actor with Stash {
  implicit val system = context.system

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  val queue = Source
    .queue[Map[String, String]](100, OverflowStrategy.backpressure)
    //.throttle(1, 3000.millisecond) //I use this to write n elements in n milliseconds
    .log(name = "myStream")
    .addAttributes(
      Attributes.logLevels(
        onElement = Attributes.LogLevels.Off,
        onFinish = Attributes.LogLevels.Error,
        onFailure = Attributes.LogLevels.Error))
    .recover {
      case e: RuntimeException =>
        logger ! (LogMessage(0, JobMessage("ERROR", e.getMessage, 99)))
        Map[String, String] ()
    }
    .via(matchFile())
    .toMat(matchRepo().asInstanceOf[Graph[SinkShape[AnyRef], Future[Serializable]]])(Keep.both)
    .run()


  override def receive = {
    case PipelineStart =>
      logger ! (LogMessage(instance, JobMessage("START", "", repo.job_configuration_id)))
      unstashAll()


    case PipelineError(ex) =>
      log.error(ex, "ERROR")
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, repo.job_configuration_id)))
      self ! PoisonPill

    case PipelineEnd =>
      logger ! (LogMessage(instance, JobMessage("END", "", repo.job_configuration_id)))
      self ! PoisonPill

    case el: Map[String, String] =>
      unstashAll()
      queue._1.offer(el).pipeTo(self)

      context.become({

        case QueueOfferResult.Enqueued    =>
          //println(s"enqueued $el")
          unstashAll()
          context.unbecome()

        case QueueOfferResult.Failure(ex) =>
          //println(s"Offer failed ${ex.getMessage}")
          logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, repo.job_configuration_id)))
          self ! PoisonPill

        case QueueOfferResult.QueueClosed =>
          //println("Source Queue closed")
          log.error(s"Output queue closed!")
          self ! PoisonPill

        case msg => stash()

      }, discardOld = false) // stack on top instead of replacing
  }

  def matchFile() : Flow[Map[String, String], AnyRef, AnyRef] = {
    entity.entity_type match {
      case "csv" =>
        log.info("DATA TRANSITION FOR CSV OUTPUT")
        Flow[Map[String, String]]
          .via(CSV.formatOutput(entity))
      case "json" =>
        log.info("DATA TRANSITION FOR JSON OUTPUT")
        Flow[Map[String, String]]
          .via(JSON.formatOutput(entity))
      /*case "xml" =>
        log.info("DATA TRANSITION FOR XML OUTPUT")
        Flow[Map[String, String]]
          .via(XML.formatOutput(entity))*/
      case _ =>
        log.error("No file recognized in output")
        Flow[Map[String, String]].map(x => x)
    }
  }

  def matchRepo() = {
    repo.repo_type match {
      case "sftp" =>
        log.info("SFTP OUTPUT CREATION")
        SftpConnector.getSink(repo, entity)

      case "ftp" =>
        log.info("FTP OUTPUT CREATION")
        FtpConnector.getSink(repo, entity)

      case "mysql" =>
        log.info("MYSQL OUTPUT CREATION")
        MySQLConnector.getSink(repo, entity)

      case "postgresql" =>
        log.info("POSTGRESQL OUTPUT CREATION")
        PostgresConnector.getSink(repo, entity)

      case "oracle" =>
        log.info("ORACLE OUTPUT CREATION")
        OracleDBConnector.getSink(repo, entity)

      case "filesystem" =>
        log.info("FILESYSTEM OUTPUT CREATION")
        FileSystemConnector.getSink(repo, entity)

      case "kafka" =>
        log.info("KAFKA OUTPUT CREATION")
        KafkaConnector.getSink(repo, entity)

      case "hdfs" =>
        log.info("HDFS OUTPUT CREATION")
        HdfsConnector.getSink(repo, entity)

      case "mongo" =>
        log.info("MONGO OUTPUT CREATION")
        MongoDBConnector.getSink(repo, entity)

      case "rabbit" =>
        log.info("RABBIT OUTPUT CREATION")
        RabbitConnector.getSink(repo, entity)
    }
  }
}
