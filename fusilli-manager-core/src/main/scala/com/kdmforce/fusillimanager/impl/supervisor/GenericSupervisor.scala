package com.kdmforce.fusillimanager.impl.supervisor

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, Router }
import scala.util.Random
import akka.actor.{Actor, ActorRef, PoisonPill, Props, Terminated}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Repo, Job, Pipeline, PipelineStop}
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import com.kdmforce.fusillimanager.impl.job._
import com.kdmforce.fusillimanager.impl.job.join_connections.BigSpawner
import play.api.libs.json.{Format, Json, JsValue,JsArray, JsObject}
import com.kdmforce.fusillimanager.impl.job.job_specific_objects.MatcherItems
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, BroadcastRoutingLogic, Router }

import scala.util.Random
import com.kdmforce.fusillimanager.impl.MessageSerializer._

object GenericSupervisor {
  def props = Props[GenericSupervisor]
}

class GenericSupervisor(instance: Int, job: Job, followers: Seq[ActorRef]) extends Actor {
  var instances: Int = job.job_instances
  if (instances < 2) {
    instances = 1
  }

  val routees = Vector.fill(instances) {
    val r = job.job_name match {
      case "concat" =>
        context.system.actorOf(Props(classOf[Concatter], instance, job,  followers(0)))

      case "rename" =>
        context.system.actorOf(Props(classOf[Renamer], instance, job,  followers(0)))

      case "replace" =>
        context.system.actorOf(Props(classOf[Replacer], instance, job,  followers(0)))

      case "split" =>
        context.system.actorOf(Props(classOf[Splitter], instance, job,  followers(0)))

      case "join" =>
        //crea connettore
        val dbConnection = BigSpawner.spawnConnection(((job.job_configs(0).config).apply(0)\"secondary_input").get.as[Repo] )
        context.system.actorOf(Props(classOf[Joiner], instance, job,  followers(0), followers(1), dbConnection))

      case "match" =>
        val cond = MatcherItems.PredicateParser(job.job_configs(0).config)
        context.system.actorOf(Props(classOf[Matcher], instance, job,  followers(0), followers(1), cond))

      case "split&clone" =>
        context.system.actorOf(Props(classOf[SplitCloner], instance, job,  followers(0)))

      case "mark" =>
        context.system.actorOf(Props(classOf[Marker], instance, job, (job.job_configs(0).config\"new_key").get.as[String], (job.job_configs(0).config\"new_value").get.as[String], followers(0)))

    }
    context.watch(r)
    ActorRefRoutee(r)
  }
  val router = Router(RoundRobinRoutingLogic(), routees)

  val killerRouter = Router(BroadcastRoutingLogic(), routees)

  var index = 0

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case PipelineStart =>
      log.info("START " + (job.job_name).toUpperCase())
      logger ! (LogMessage(instance, JobMessage("START", "", job.job_configuration_id)))
      for (follower <- followers) { follower ! PipelineStart }

    /*case SupervisedAck =>
      log.info("\n\n\nwhaaaaaaaaaaaaaaat")*/

    case PipelineEnd =>
      log.info("\n\n\nSTOP PROCEDURE OF " + (job.job_name).toUpperCase() + "'S SUPERVISOR INITIALIZED")
      var aliveSons = instances
      killerRouter.route((SupervisorStop, self), sender())
      context.become({
                        case SupervisedAck =>
                                  //log.info("\n\n\nwoooo")
                                  aliveSons-=1
                                  if(aliveSons ==0){
                                      logger ! (LogMessage(instance, JobMessage("END", "", job.job_configuration_id)))
                                      for (follower <- followers) { follower ! PipelineEnd }
                                      log.info("\nSTOP PROCEDURE OF " + (job.job_name).toUpperCase() + "'S SUPERVISOR SUCCESSFULLY CONCLUDED! CONGRATS!")
                                      self ! PoisonPill
                                  }
                                  else log.info(aliveSons + " more Ack needed")
                        case _ => log.error("\n\n\n\nUnexpected message received at " + job.job_name +"'s supervisor during stop routine")
                      })

    case PipelineError(ex) =>
      log.error(ex, "ERROR ON " + (job.job_name).toUpperCase())
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, job.job_configuration_id)))
      for (follower <- followers){ follower ! PipelineError(ex) }
      self ! PoisonPill

    case el: Map[String, String] =>
      router.route(el, sender())

  }
}

