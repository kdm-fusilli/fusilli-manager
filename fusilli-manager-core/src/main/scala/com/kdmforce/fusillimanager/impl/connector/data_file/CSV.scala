package com.kdmforce.fusillimanager.impl.connector.data_file

import java.nio.charset.StandardCharsets

import akka.NotUsed
import akka.stream.alpakka.csv.scaladsl.{CsvFormatting, CsvParsing, CsvToMap}
import akka.stream.scaladsl.{Flow, Source}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Entity
import com.kdmforce.fusillimanager.impl.connector.DataFile
import org.slf4j.{Logger, LoggerFactory}


object CSV extends DataFile[ByteString, String] {

  private final val log: Logger = LoggerFactory.getLogger("CSV")

  override def formatInput(entity: Entity) = {
    log.info("START METHOD FORMATINPUT")

    val header = entity.attributes.map(_.attr_name)

    val outputMap = mapOnlyHeaders(header) _

    val separator = entity.separator match {
      case "\\" => CsvParsing.Backslash
      case ":" => CsvParsing.Colon
      case "," => CsvParsing.Comma
      case "\"" => CsvParsing.DoubleQuote
      case ";" => CsvParsing.SemiColon
    }

    log.info("START CSV PARSING")
    CsvParsing.lineScanner(separator)
      .via(CsvToMap.toMapAsStrings(StandardCharsets.UTF_8))
      .map(outputMap)
  }

  override def formatOutput(entity: Entity) = {
    log.info("START METHOD FORMATOUTPUT")
    val header = entity.attributes.map(_.attr_name)

    log.info("START CSV PARSING")
    Flow[Map[String, String]]
      .map(header.map)
      .prepend(Source.single(header))
      .via(CsvFormatting.format((entity.separator.toCharArray).apply(0)))

  }

  def mapOnlyHeaders(header: Seq[String]) (doc:Map[String,String]) : Map[String,String] = {
    var outMap = Map[String,String]()

    for (keyholder<-header){
      outMap += (keyholder -> doc(keyholder))
    }
    outMap
  }

}
