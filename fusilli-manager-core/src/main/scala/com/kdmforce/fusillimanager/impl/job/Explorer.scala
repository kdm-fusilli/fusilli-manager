package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging

import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{ConfigValue, Job}
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Attribute}
import com.kdmforce.fusillimanager.api.DiscoveryEndpointSerializer.{DiscoRepo, DiscoEntity, DiscoAttribute, DiscoRepoOut}

import slick.jdbc.{GetResult, JdbcBackend}
import slick.jdbc.JdbcBackend.Database
import slick.util.AsyncExecutor
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.JdbcBackend.Database
import scala.concurrent.{Future, Await}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Success, Failure}
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

object Explorer {
  def props = Props[Concatter]
}

class Explorer(autodiscovery: Int, job: DiscoRepo, follower: ActorRef) extends Actor {
  var index = 0
  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case PipelineStart =>
      log.info("START EXPLORER")
//      logger ! (LogMessageDiscovery(autodiscovery, JobMessage("START", "", job.repo_id)))
      follower ! PipelineStart

    case PipelineEnd =>
      log.info("END EXPLORER")
//      logger ! (LogMessageDiscovery(autodiscovery, JobMessage("END", "", job.repo_id)))
      follower ! PipelineEnd
      self ! PoisonPill

    case PipelineError(ex) =>
      log.error(ex, "ERROR ON EXPLORER")
//      logger ! (LogMessageDiscovery(autodiscovery, JobMessage("ERROR", ex.getMessage, job.repo_id)))
      follower ! PipelineError(ex)
      self ! PoisonPill

    case a: (SupervisorStop,ActorRef) => 
      a._2 ! SupervisedAck

    case el: Map[String,String] =>
      if (el("condition") == "GO!") {
        job.repo_type match {
          case "postgresql" =>
            index = index + 1
            log.info("EXPLORER OF " + s"${index}" + " ELEMENT")
            //logger ! (LogMessage(autodiscovery, JobMessage("EXPLORER RESULT -> " + s"${el}", "", job.repo_id)))
            follower ! connectionToPostgres(job)
        }
      }

      def connectionToPostgres(job: DiscoRepo) = {
        val db =
          Database.forURL(
            s"jdbc:postgresql://${job.host}:${job.port}/${job.schema_path}",
            job.repo_user,
            job.repo_password,
            driver = "org.postgresql.Driver",
            executor = AsyncExecutor.default("PostgresExecutor", 20))


        val profile = slick.jdbc.PostgresProfile


        val query = db.run(sql"""SELECT table_name FROM information_schema.tables WHERE table_schema='public'""".as[(String)])

        val tables: Seq[String] = Await.result(query, 5.seconds)

        val results = tables.map {
          table =>
            db.run(for {
              metaTables <- slick.jdbc.meta.MTable.getTables(table)
              columns <- metaTables.head.getColumns
            } yield columns.map { e => (e.table.name, e.name, e.sqlTypeName)}) //foreach println
        }

        var roba = Seq[DiscoEntity]()

        for(future <- results) {

          var table = Await.result(future, Duration.Inf)

          var tmp = Seq[DiscoAttribute]()
          var entityName = ""

          for (colomn <- table) {

            tmp = tmp :+ DiscoAttribute(colomn._2, colomn._3.get, "", "")
            entityName = colomn._1
          }

          roba = roba :+ DiscoEntity(entityName, "sql", "", tmp)
        }

        new DiscoRepoOut(job.repo_id, roba)
      }
  }
}
