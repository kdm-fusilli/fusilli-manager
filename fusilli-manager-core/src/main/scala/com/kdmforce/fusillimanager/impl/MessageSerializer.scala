package com.kdmforce.fusillimanager.impl

import play.api.libs.json.{Format, Json}


object MessageSerializer {

  case class NewInstanceLog(instance_id: Int, instance_data: LogInstanceData)
  object NewInstanceLog{
    implicit val format: Format[NewInstanceLog] = Json.format
  }

  case class DeleteInstanceLog(instance_id: Int)
  object DeleteInstanceLog {
    implicit val format: Format[DeleteInstanceLog] = Json.format
  }

  case class LogMessage(instance_id: Int, job_message: JobMessage)
  object LogMessage {
    implicit val format: Format[LogMessage] = Json.format
  }

  case class LogInstanceData(logUrl: String, policies_id: String, pipeline_id: Int)
  object LogInstanceData{
    implicit val format: Format[LogInstanceData] = Json.format
  }

  case class JobMessage(status: String, message: String, job_id: Int)
  object JobMessage{
    implicit val format: Format[JobMessage] = Json.format
  }

  /*case class NewDiscoveryLog(autodiscovery_id: Int, autodiscovery_data: LogAutoDiscoveryData)
  object NewDiscoveryLog{
    implicit val format: Format[NewDiscoveryLog] = Json.format
  }

  case class DeleteDiscoveryLog(autodiscovery_id: Int)
  object DeleteDiscoveryLog {
    implicit val format: Format[DeleteDiscoveryLog] = Json.format
  }

  case class LogMessageDiscovery(autodiscovery_id: Int, job_message: JobMessage)
  object LogMessageDiscovery {
    implicit val format: Format[LogMessageDiscovery] = Json.format
  }

  case class LogAutoDiscoveryData(logUrl: String, repo_id: Int)
  object LogAutoDiscoveryData{
    implicit val format: Format[LogAutoDiscoveryData] = Json.format
  }*/

  case class PipelineStart()

  case class PipelineEnd()

  case class SupervisorStop()

  case class SupervisedAck()

  final case class PipelineError(ex: Throwable)
  
  case class PipelineAck()

}
