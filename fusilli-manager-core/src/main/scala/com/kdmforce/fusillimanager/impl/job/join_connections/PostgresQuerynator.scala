package com.kdmforce.fusillimanager.impl.job.join_connections

import akka.stream.alpakka.slick.scaladsl.{Slick, SlickSession}
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.slf4j.{Logger, LoggerFactory}
import slick.dbio.DBIO
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.{GetResult, JdbcBackend}
import slick.util.AsyncExecutor

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}

class PostgresQuerynator(repoData: Repo, outAttrz : Seq[String], connection : Any) extends Querynator {

  private final val log: Logger = LoggerFactory.getLogger("POSTGRESQL QUERYNATOR")

  def queryResult(matchings: Map[String, Any]) = {

    val dbAndSchema = repoData.schema_path.split("/")
    var db = ""
    var schema = ""
    if(dbAndSchema.size == 2) {
      db = dbAndSchema(0)
      schema = dbAndSchema(1) + "."
    } else {
      db = repoData.schema_path
    }

    val settings = connection.asInstanceOf[JdbcBackend.DatabaseDef]
    log.info(s"START CONNECTION ${settings.toString}")

    val profile = slick.jdbc.PostgresProfile

    implicit val session: SlickSession = SlickSession.forDbAndProfile(settings, profile)

    implicit val resultAsStringMap = GetResult[Map[String, Any]] ( prs =>
      (1 to prs.numColumns).map(_ =>
        prs.rs.getMetaData.getColumnName(prs.currentPos+1) -> prs.nextObject.toString
      ).toMap
    )

    var valueOfMatch: String = ""
    for((k, v) <- matchings) {
      valueOfMatch = k + " = \'" + v + "\',"
    }

    import session.profile.api._

    val query = sql"""SELECT #${outAttrz.mkString(",")} FROM #${schema + repoData.entities(0).entity_name} WHERE #${valueOfMatch.dropRight(1)}""".as[Map[String, Any]]

    val queryResult = settings.run(query)

    val result = Await.result(queryResult, 5.seconds)
    log.info(s"QUERY WITH SUCCESS")

    result.toList
  }

}
