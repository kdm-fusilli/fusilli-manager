package com.kdmforce.fusillimanager.impl

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Policy, PolicyAttribute}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, LogMessage, PipelineAck, PipelineEnd, PipelineError, PipelineStart}
import com.roundeights.hasher.Implicits._

object Policier {
  def props = Props[Policier]
}

class Policier(instance: Int, policy: Policy, follower: ActorRef) extends Actor {

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {

    case PipelineStart =>
      log.info("START PIPELINE")
      logger ! (LogMessage(instance, JobMessage("START", "", policy.entity_id)))
      sender() ! PipelineAck
      follower ! PipelineStart

    case PipelineEnd =>
      log.info("END PIPELINE")
      logger ! (LogMessage(instance, JobMessage("END", "", policy.entity_id)))
      follower ! PipelineEnd
      self ! PoisonPill


    case PipelineError(ex) =>
      log.error(ex, "ERROR")
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, policy.entity_id)))
      follower ! PipelineError(ex)
      self ! PoisonPill

    case el: Map[String, String] =>
      log.info(s"RECEIVED PIPELINE: ${el}")
      follower ! policyFun(el, policy.attrs)
      sender() ! PipelineAck
  }

  def encryption(value: String, parameter: String): String = {
    try {
      // controlla se la lunghezza del sale è compresa tra 6 e 12
      require(6 to 12 contains (parameter.length))
      // se soddisfa il requisito precedente allora applica l'algoritmo SHA256 a valore indicato da criptare
      value.salt(parameter).sha256.hex
    }
    catch {
      // se il requisito della lunghezza del sale non viene rispettata logga l'errore e non cripta il valore
      case _: Throwable => log.error(s"Salt must have between 6 to 12 characters")
        value
    }
  }

    def tokenization(value: String, parameter: Int): String = {
      val maxLeng = value.length

      //prende un carattere a caso dalla stringa
      val refString: String = "0123456789"
      //crea uno stringbuffer di lungheza 'parameter'
      val sb: StringBuilder = new StringBuilder()
      if (parameter > maxLeng){
        for (i <- 0 until maxLeng) {
          //da 0 a 'maxLeng'
          val index: Int = (refString.length * Math.random()).toInt
          //aggiunge un carattere alla volta alla fine di sb
          sb.append(refString.charAt(index))
        }
      } else {
        for (i <- 0 until parameter) {
          //da 0 a 'parameter'
          val index: Int = (refString.length * Math.random()).toInt
          //aggiunge un carattere alla volta alla fine di sb
          sb.append(refString.charAt(index))
        }
      }
      //torna sb come stringa
      sb.toString
    }

  def policyFun(doc: Map[String, String], policyAttributes: Seq[PolicyAttribute]): Map[String, String] = {
    var newDoc = doc
    for (p <- policyAttributes) {
      p.policy_behaviour match {
        case "tokenization" =>
            val tmp = tokenization(p.attr_name, p.parameter.toInt)
          newDoc = newDoc - p.attr_name + (p.attr_name -> tmp)
        case "encryption" =>
          val tmp = encryption(p.attr_name, p.parameter)
          newDoc = newDoc - p.attr_name + (p.attr_name -> tmp)
        case _ =>
          log.error(s"Policy behaviour not recognized: ${p.policy_behaviour}")
          self ! PoisonPill
      }
    }
    newDoc
  }
}