package com.kdmforce.fusillimanager.impl.connector.connection

import java.util
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.slf4j.{Logger, LoggerFactory}
import akka.stream.alpakka.amqp._
import akka.stream.alpakka.amqp.scaladsl.{AmqpFlow, AmqpRpcFlow, AmqpSink, AmqpSource, CommittableReadResult}
import scala.concurrent.duration._
import scala.collection.immutable
import scala.concurrent.Future
import akka.{Done, NotUsed}
import io._

object RabbitConnector extends Connector[Any]{

  private final val log: Logger = LoggerFactory.getLogger("RABBIT")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    val amqpSource = spawnSourceConnection(repo)
    log.info(s"START CONNECTION")

    amqpSource
      .map { msg =>
        msg.bytes
      }
  }


  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")

    val AmqpSink = spawnWriteConnection(repo)
    log.info(s"START CONNECTION")

    val jsonString = createRecordRabbitWriter _
    log.info(s"RECORD WRITER COMPLETED")


    AmqpSink.contramap(msg =>
      jsonString(msg.asInstanceOf[ByteString]))
  }

  def spawnSourceConnection(repo: Repo) = {

    AmqpSource.atMostOnceSource(
      NamedQueueSourceSettings(AmqpUriConnectionProvider(s"amqp://${repo.repo_user}:${repo.repo_password}@${repo.host}:${repo.port}"), repo.schema_path)
        .withDeclaration(QueueDeclaration(repo.schema_path))
        .withAckRequired(false),
      bufferSize = Int.MaxValue
    )
  }

  def spawnWriteConnection(repo: Repo) = {

    AmqpSink.simple(
      AmqpWriteSettings(AmqpUriConnectionProvider(s"amqp://${repo.repo_user}:${repo.repo_password}@${repo.host}:${repo.port}"))
        .withRoutingKey(repo.schema_path)
        .withDeclaration(QueueDeclaration(repo.schema_path))
    )
  }

  def createRecordRabbitWriter(thing: ByteString) = {

    ByteString(thing.utf8String)

  }

}


