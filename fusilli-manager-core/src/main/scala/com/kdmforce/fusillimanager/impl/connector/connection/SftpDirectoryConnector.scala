package com.kdmforce.fusillimanager.impl.connector.connection

import java.net.InetAddress
import java.nio.file._

import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Sftp
import akka.stream.alpakka.ftp.{FtpCredentials, SftpSettings}
import akka.stream.scaladsl.FileIO
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import akka.stream.alpakka.file.scaladsl.Directory
import org.slf4j.{Logger, LoggerFactory}
import akka.stream.scaladsl.Source
import scala.concurrent.Future


//DA TESTARE
object SftpDirectoryConnector extends Connector[ByteString]{

  private final val log: Logger = LoggerFactory.getLogger("SFTP DIRECTORY")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    val fs = FileSystems.getDefault
    log.info("START CONNECTION")
    val settings = spawnConnection(repo)

    log.info(s"DIRECTORY CONNECTION ${repo.schema_path.toString}")
    Sftp
      .ls(repo.schema_path, settings)
      .flatMapConcat(sftpFile => {
        if(sftpFile.path.endsWith(entity.entity_type)) {
          Sftp.fromPath(sftpFile.path, settings).reduce((a, b) => a ++ b)
        } else {
          Source.empty
        }
      })
  }

  //da fare
  override def getSink(repo: Repo, entity: Entity) = {

    val file = Paths.get(repo.schema_path + entity.entity_name)

    FileIO.toPath(file)

  }

  def spawnConnection (repo: Repo): SftpSettings = {
    val credentials = FtpCredentials.create(repo.repo_user, repo.repo_password)

    SftpSettings
      .create(InetAddress.getByName(repo.host))
      .withPort(repo.port)
      .withCredentials(credentials)
      .withStrictHostKeyChecking(false)
  }

}

