package com.kdmforce.fusillimanager.impl.connector

import akka.actor.{Actor, ActorRef, PoisonPill, Props, Stash,Kill}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import akka.pattern.pipe
import akka.stream._
import akka.stream.scaladsl.{Flow, Keep, Sink, Source, SourceQueue}
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo,Attribute}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, LogMessage, _}
import com.kdmforce.fusillimanager.impl.connector.connection._
import com.kdmforce.fusillimanager.impl.connector.data_file._
import scala.concurrent.duration._
import scala.language.postfixOps


import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ConnectorIn{

  def props = Props[ConnectorIn]

  case class Start()

}

class ConnectorIn(instance:Int, repo: Repo, entity: Entity, follower: ActorRef) extends Actor {
  implicit val system = context.system

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case ConnectorIn.Start =>
      logger ! (LogMessage(instance, JobMessage("START", "", repo.job_configuration_id)))
      sender() ! PipelineAck
      follower ! PipelineStart

    case PipelineEnd =>
      logger ! (LogMessage(instance, JobMessage("END", "", repo.job_configuration_id)))
      follower ! PipelineEnd
      self ! PoisonPill


    case PipelineError(ex) =>
      log.error(ex, "ERROR")
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, repo.job_configuration_id)))
      follower ! PipelineError(ex)
      self ! PoisonPill

    case el: Map[_, _] =>
      Thread.sleep(repo.delay)
      follower ! el
      sender() ! PipelineAck

    case PipelineStart  =>

      val inputEntity: Source[Map[String, String], Any]  = repo.repo_type match {
        case "sftp" =>
          entity.entity_type match {
            case "csv" =>
              log.info("SFTP INPUT CREATION AND DATA TRANSITION FOR CSV INPUT")
              SftpConnector.getSource(repo, entity)
                .via(CSV.formatInput(entity))
            case "json" =>
              log.info("SFTP INPUT CREATION AND DATA TRANSITION FOR JSON INPUT")
              SftpConnector.getSource(repo, entity)
                .via(JSON.formatInput(entity))
            case "xml" =>
              log.info("SFTP INPUT CREATION AND DATA TRANSITION FOR XML INPUT")
              SftpConnector.getSource(repo, entity)
                .via(XML.formatInput(entity))
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "ftp" =>
          entity.entity_type match {
            case "csv" =>
              log.info("FTP INPUT CREATION AND DATA TRANSITION FOR CSV INPUT")
              FtpConnector.getSource(repo, entity)
                .via(CSV.formatInput(entity))
            case "json" =>
              log.info("FTP INPUT CREATION AND DATA TRANSITION FOR CSV JSON")
              FtpConnector.getSource(repo, entity)
                .via(JSON.formatInput(entity))
            case "xml" =>
              log.info("FTP INPUT CREATION AND DATA TRANSITION FOR XML INPUT")
              FtpConnector.getSource(repo, entity)
                .via(XML.formatInput(entity))
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "mysql" =>
          log.info("MYSQL INPUT CREATION")
          MySQLConnector.getSource(repo, entity)

        case "postgresql" =>
          log.info("POSTGRESQL INPUT CREATION")
          PostgresConnector.getSource(repo, entity)

        case "oracle" =>
          log.info("ORACLE INPUT CREATION")
          OracleDBConnector.getSource(repo, entity)

        case "filesystem" =>
          entity.entity_type match {
            case "csv" =>
              log.info("FILESYSTEM INPUT CREATION AND DATA TRANSITION FOR CSV INPUT")
              FileSystemConnector.getSource(repo, entity)
                .via(CSV.formatInput(entity))
            case "json" =>
              log.info("FILESYSTEM INPUT CREATION AND DATA TRANSITION FOR JSON INPUT")
              FileSystemConnector.getSource(repo, entity)
                .via(JSON.formatInput(entity))
            case "xml" =>
              log.info("FILESYSTEM INPUT CREATION AND DATA TRANSITION FOR XML INPUT")
              FileSystemConnector.getSource(repo, entity)
                .via(XML.formatInput(entity))
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "kafka" =>
          entity.entity_type match {
            case "json" =>
              log.info("KAFKA INPUT CREATION AND DATA TRANSITION FOR JSON INPUT")
              KafkaConnector.getSource(repo, entity)
                .via(JSON.formatInput(entity))
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "hdfs" =>
          entity.entity_type match {
            case "csv" =>
              log.info("HDFS INPUT CREATION AND DATA TRANSITION FOR CSV INPUT")
              HdfsConnector.getSource(repo, entity)
                .via(CSV.formatInput(entity))
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "mongo" =>
          entity.entity_type match {
            case "json" =>
              log.info("MONGO INPUT CREATION AND DATA TRANSITION FOR JSON INPUT")
              MongoDBConnector.getSource(repo, entity)
                .via(JSON.formatInput(entity))
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "fsdirectory" =>
          entity.entity_type match {
            case "xml" =>
              log.info("FILESYSTEM DIRECTORY INPUT CREATION AND DATA TRANSITION FOR XML INPUT")
              if ((entity.attributes.map(_.attr_type)).contains("Filename")){
                FSDirectoryConnectorWithNames.getSource(repo, entity)
                  .via(XMLWithNames.formatInput(entity))
              }
              else{
                FSDirectoryConnector.getSource(repo, entity)
                  .via(XML.formatInput(entity))
              }
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "ftpdirectory" =>
          entity.entity_type match {
            case "xml" =>
              log.info("FTP DIRECTORY INPUT CREATION AND DATA TRANSITION FOR XML INPUT")
              if ((entity.attributes.map(_.attr_type)).contains("Filename")){
                FtpDirectoryConnectorWithNames.getSource(repo, entity)
                  .via(XMLWithNames.formatInput(entity))
              }
              else {
                FtpDirectoryConnector.getSource(repo, entity)
                  .via(XML.formatInput(entity))
              }
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "sftpdirectory" =>
          entity.entity_type match {
            case "xml" =>
              log.info("SFTP DIRECTORY INPUT CREATION AND DATA TRANSITION FOR XML INPUT")
              if ((entity.attributes.map(_.attr_type)).contains("Filename")){
                SftpDirectoryConnectorWithNames.getSource(repo, entity)
                  .via(XMLWithNames.formatInput(entity))
              }
              else {
                SftpDirectoryConnector.getSource(repo, entity)
                  .via(XML.formatInput(entity))
              }
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }

        case "rabbit" =>
          entity.entity_type match {
            case "json" =>
              log.info("RABBIT INPUT CREATION AND DATA TRANSITION FOR JSON INPUT")
              RabbitConnector.getSource(repo, entity)
                .via(JSON.formatInput(entity))
            case _ =>
              log.error("No file recognized in input")
              Source.empty
          }
      }

      inputEntity
        .runWith(Sink.actorRefWithBackpressure(
          self,
          onInitMessage = ConnectorIn.Start,
          ackMessage = PipelineAck,
          onCompleteMessage = PipelineEnd,
          onFailureMessage = PipelineError
        ))
  }
}