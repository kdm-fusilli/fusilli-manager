package com.kdmforce.fusillimanager.impl.connector.connection

import akka.stream.alpakka.slick.scaladsl.{Slick, SlickSession}
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.slf4j.{Logger, LoggerFactory}
import slick.dbio.DBIO
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.{GetResult, JdbcBackend}
import slick.util.AsyncExecutor

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

object MySQLConnector extends Connector[Any]{

  private final val log: Logger = LoggerFactory.getLogger("MYSQL")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    val dbAndSchema = repo.schema_path.split("/")
    var db = ""
    var schema = ""
    if(dbAndSchema.size == 2) {
      db = dbAndSchema(0)
      schema = dbAndSchema(1) + "."
    } else {
      db = repo.schema_path
    }

    val settings = spawnConnection(repo, db)
    log.info(s"START CONNECTION ${settings.toString}")

    val profile = slick.jdbc.MySQLProfile

    implicit val session: SlickSession = SlickSession.forDbAndProfile(settings, profile)

    implicit val resultAsStringMap = GetResult[Map[String,String]] ( prs =>
      (1 to prs.numColumns).map(_ =>
        prs.rs.getMetaData.getColumnName(prs.currentPos+1) -> prs.nextObject.toString
      ).toMap
    )

    val attrs = entity.attributes.map(attr => attr.attr_name)

    import session.profile.api._

    log.info("START SELECT")
    Slick
      .source(sql"""SELECT #${attrs.mkString(",")} FROM #${schema + entity.entity_name}""".as[Map[String,String]])
  }

  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")

    val dbAndSchema = repo.schema_path.split("/")
    var db = ""
    var schema = ""
    if(dbAndSchema.size == 2) {
      db = dbAndSchema(0)
      schema = dbAndSchema(1) + "."
    } else {
      db = repo.schema_path
    }

    val settings = spawnConnection(repo, db)
    log.info(s"START CONNECTION ${settings.toString}")

    val profile = slick.jdbc.MySQLProfile


    implicit val session: SlickSession = SlickSession.forDbAndProfile(settings, profile)

    import session.profile.api._

    implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

    val attrsInsert = entity.attributes.map(attr => (attr.attr_name, attr.attr_type))

    val query = s"""CREATE TABLE IF NOT EXISTS ${schema + entity.entity_name} (${attrsInsert.map(elem => "%s %s".format(elem._1, elem._2)).mkString(", ")})"""



    val queryResult = settings.run(sqlu"#$query")
    Await.result(queryResult, 5.seconds)
    log.info(s"QUERY WITH SUCCESS")

    val sqlConstructor = insertElem(entity, schema) _
    log.info(s"INSERT ELEMENT")

    Slick.sink(elem => sqlConstructor(elem.asInstanceOf[Map[String, String]]))

  }


  def spawnConnection (repo: Repo, db: String) : JdbcBackend.DatabaseDef = {
    Database.forURL(
      s"jdbc:mysql://${repo.host}:${repo.port}/${db}",
      repo.repo_user,
      repo.repo_password,
      driver = "com.mysql.cj.jdbc.Driver",
      executor = AsyncExecutor.default("MysqlExecutor", 20))
  }

  def insertElem(item: Entity, schema: String)(thing: Map[String, String])(implicit session: SlickSession): DBIO[Int] = {
    import session.profile.api._

    var insertContent = s"""INSERT INTO ${schema + item.entity_name}(${item.attributes.map(_.attr_name).mkString(",")}) VALUES ("""

    for (element <- item.attributes) {
      element.attr_type match {
        case "INTEGER" => insertContent += s" ${thing.get(element.attr_name).orNull}, "
        case _ => insertContent += s" '${thing.getOrElse(element.attr_name, "").asInstanceOf[String].replace("\'", "\'\'")}', "
      }
    }
    insertContent = insertContent.patch(insertContent.lastIndexOf(','), " )", 1)
    sqlu"#$insertContent"
  }

}
