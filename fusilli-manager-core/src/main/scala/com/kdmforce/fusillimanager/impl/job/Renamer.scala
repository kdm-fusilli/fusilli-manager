package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{ConfigValue, Job}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, LogMessage, PipelineAck, PipelineEnd, PipelineError, PipelineStart,SupervisorStop, SupervisedAck}

object Renamer {
  def props = Props[Renamer]
}

class Renamer(instance: Int, job: Job, follower: ActorRef) extends Actor {
  var index = 0
  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case a: (SupervisorStop,ActorRef) => 
      a._2 ! SupervisedAck

    case el: Map[String, String] =>
      val result = renameFun(el, job.job_configs)
      index = index + 1
      log.info("RENAMER OF " + s"${index}" + " ELEMENT")
      //logger ! (LogMessage(instance, JobMessage("RENAMER RESULT: " + s"${result}", "", job.job_configuration_id)))
      follower ! result
  }

  def renameFun(doc: Map[String, String], jobConfigs: Seq[ConfigValue]): Map[String, String] = {
    var newDoc = doc
    for (j <- jobConfigs) {
      val newValue = newDoc(j.input_attrs(0))
      newDoc = newDoc - j.input_attrs(0) + (j.output_attrs(0) -> newValue)
    }
    newDoc
  }
}

