package com.kdmforce.fusillimanager.impl


import com.kdmforce.fusillimanager.api.FusilliManagerService
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Pipeline, PipelineStop}
import com.kdmforce.fusillimanager.api.DiscoveryEndpointSerializer.DiscoveryDoc
import com.kdmforce.fusillimanager.api.DiscoveryEndpointSerializer.DiscoRepoOut
import akka.Done
import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings, ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import akka.pattern.ask
import akka.util.Timeout
import com.lightbend.lagom.scaladsl.api.ServiceCall

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Success
import scala.util.Failure



/**
 * Implementation of the FusilliManagerService.
 */
class FusilliManagerServiceImpl(system: ActorSystem) extends FusilliManagerService {

  private val log = Logging.getLogger(system, this)

  val dispatcher = system.actorOf(
    ClusterSingletonManager.props(
      singletonProps = Props(classOf[WorkDispatcher]),
      terminationMessage = PoisonPill,
      settings = ClusterSingletonManagerSettings(system)),
    name = "dispatcher")

  val dispatcherProxy = system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/dispatcher",
      settings = ClusterSingletonProxySettings(system))
    )


  override def pipelineStart(): ServiceCall[Pipeline, Done] = ServiceCall { request =>
    implicit val timeout = Timeout(5.seconds)
    (dispatcherProxy ? request).mapTo[Done]
    .onComplete {
      case Success(_) =>  log.info("DISPATCHER TOOK CHARGE THIS REQUEST (" + s"${request}" + ") WITH SUCCESS")
      case Failure(er) => log.info(s"ERROR (${er}) WHILE PROCESSING THIS REQUEST (${request})")
    }
    Future.successful(Done)

  }

  override def discoveryStart(): ServiceCall[DiscoveryDoc, Done] = ServiceCall { request =>
    implicit val timeout = Timeout(5.seconds)
    (dispatcherProxy ? request).mapTo[Done]
      .onComplete {
        case Success(_) => log.info("DISCOVERY TAKED CHARGE THIS REQUEST (" + s"${request}" + ") WITH SUCCESS")
        case Failure(er) => log.info(s"ERROR (${er}) WHILE PROCESSING THIS REQUEST (${request})")
      }
    Future.successful(Done)
  }

  override def postme(): ServiceCall[String, Done] = ServiceCall { request =>
    implicit val timeout = Timeout(5.seconds)
    log.info(request)
    Future.successful(Done)
  }

  override def pipelineStop(): ServiceCall[Pipeline, Done] = ServiceCall { request =>
    implicit val timeout = Timeout(5.seconds)
    val alt = new PipelineStop(request.pipeline_id)
    (dispatcherProxy ? alt).mapTo[Done]
    .onComplete {
      case Success(_) =>  log.info("DISPATCHER TAKED CHARGE THIS REQUEST (" + s"${request}" + ") WITH SUCCESS")
      case Failure(er) => log.info(s"ERROR (${er}) WHILE PROCESSING THIS REQUEST (${request})")
    }
    Future.successful(Done)

  }

}
