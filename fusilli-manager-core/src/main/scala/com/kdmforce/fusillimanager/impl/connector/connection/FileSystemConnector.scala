package com.kdmforce.fusillimanager.impl.connector.connection

import java.net.InetAddress
import java.nio.file.Paths

import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Ftp
import akka.stream.alpakka.ftp.{FtpCredentials, FtpSettings}
import akka.stream.scaladsl.FileIO
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future


object FileSystemConnector extends Connector[ByteString]{

  private final val log: Logger = LoggerFactory.getLogger("FILESYSTEM")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    val file = Paths.get(repo.schema_path + entity.entity_name)

    log.info(s"FILE CONNECTION ${file}")
    FileIO.fromPath(file)

  }

  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")

    val file = Paths.get(repo.schema_path + entity.entity_name)

    log.info(s"FILE CONNECTION ${file}")
    FileIO.toPath(file)

  }
  
}

