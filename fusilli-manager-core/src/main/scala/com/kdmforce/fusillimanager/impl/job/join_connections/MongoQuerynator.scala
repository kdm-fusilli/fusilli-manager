package com.kdmforce.fusillimanager.impl.job.join_connections

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{ConfigValue, Job}
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.slf4j.{Logger, LoggerFactory}
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import akka.NotUsed
import akka.stream.IOResult


import scala.concurrent.Await
import scala.concurrent._
import org.mongodb.scala._
import org.mongodb.scala.MongoClient
import org.mongodb.scala.bson.codecs.Macros._
import play.api.libs.json.{JsValue, Json, Writes, JsObject}
import scala.concurrent.duration._
import org.mongodb.scala.model.Filters._
import scala.util.parsing.json.JSONObject



class MongoQuerynator(repoData: Repo, outAttrz : Seq[String], connection : Any) extends Querynator {

  private final val log: Logger = LoggerFactory.getLogger("MONGO QUERYNATOR")

  def queryResult(matchings: Map[String, Any]) = {

    val mongoSettings = connection.asInstanceOf[MongoClient]
    log.info(s"START CONNECTION")
    log.info(s"looking for : ${JSONObject(matchings)}" )
    val result = findElem(mongoSettings, matchings)
    log.info(s"ELEMENT FOUND : ${result}" )

    mapJsonToMap(result)

  }

  def findElem(mongoSettings: MongoClient, matchings: Map[String, Any]) = {

    val db = mongoSettings.getDatabase(repoData.schema_path)
    val collection = db.getCollection(repoData.entities(0).entity_name.toString)

    val resultQuery = collection.find(Document(JSONObject(matchings).toString())).map(dbo => dbo.toJson)
    Await.result(resultQuery.toFuture(), Duration.Inf)
  }

  def mapJsonToMap (result: Seq[String]) : List[Map[String,String]] = {
    var outList = List[Map[String, String]]()

    for (s <-result){
      val elaboratedS = Json.parse(s)
      val outMap = for(k<-outAttrz)yield(k->((elaboratedS\k).get.as[String]))
      outList = outMap.toMap :: outList
    }
    outList
  }



}
