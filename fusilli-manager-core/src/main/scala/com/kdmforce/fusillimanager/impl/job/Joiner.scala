package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{ConfigValue, Job}
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Repo
import play.api.libs.json.{Format, Json, JsValue,JsArray, JsObject}
import org.mongodb.scala.MongoClient
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._
import scala.concurrent.Await
import scala.concurrent.duration._
import com.kdmforce.fusillimanager.impl.job.join_connections._
import slick.jdbc.{GetResult, JdbcBackend}
import scala.util.control.Breaks._


object Joiner {
  def props = Props[Joiner]
}

class Joiner(instance: Int, job: Job, follower: ActorRef, errorBranch : ActorRef, dbConnection : Any) extends Actor {
  var index = 0

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  val configuration = job.job_configs(0).config
  val fieldList = (configuration.apply(0)\"matching_fields").get.as[List[JsObject]]
  val repoData: Repo = ((job.job_configs(0).config).apply(0)\"secondary_input").get.as[Repo]
  val outAttrz = for (singleAttr<-repoData.entities(0).attributes) yield (singleAttr.attr_name)

  val querynator = repoData.repo_type match {
    case "mongo" => new MongoQuerynator(repoData, outAttrz, dbConnection)
    case "postgresql" => new PostgresQuerynator(repoData, outAttrz, dbConnection)
  }


  override def receive = {

    case a: (SupervisorStop,ActorRef) => 
      a._2 ! SupervisedAck

    case el: Map[String, String] =>
      var matchings = getMatchings(el,fieldList)
      val joinDocs = querynator.queryResult(matchings)
      //println ("\n\n\n\n\nFaccio roba co " + repoData.repo_type)

      if (joinDocs.isEmpty) errorBranch ! el
      else {
        for(mergeCandidate<-joinDocs) {
          val sendMe = mergetor(el, mergeCandidate, matchings)

          index = index + 1
          log.info("JOINER OF " + s"${index}" + " ELEMENT")

          /*val result = "Main: "+ s"${el}" + "\nMerger: " + s"${mergeCandidate}" + "\nResult: " + s"s${sendMe}"
          logger ! (LogMessage(instance, JobMessage("JOINER RESULT: " + s"${result}", "", job.job_configuration_id)))*/

          follower ! sendMe
        }
      }
  }


  def mergetor( doc1: Map[String,Any], doc2 : Map[String,Any], aliases : Map[String,String] ) : Map[String,Any] = {
    //restituisci documento 1 + documento 2 meno le key-value in documento due elencate in aliases
    var output = doc1
    var exclude_us = Set[String]()
    for ((k,v) <- aliases){
      exclude_us += k
    }
    for((k,v)<-doc2){
      if (!exclude_us.contains(k)){
        output+=(k->v)
      }

    }
    output
  }

  def getMatchings(doc:Map[String,Any], fieldList : List[JsValue]) : Map[String,String] ={
    var matchings = Map[String,String]()
    for (condition <- fieldList){
      val firstKey = (condition\"first_one").get.as[String]
      val secondKey = (condition\"second_one").get.as[String]
      val matchValue = (doc(firstKey)).toString

      matchings+=(secondKey->matchValue)
    }
    matchings

  }
}