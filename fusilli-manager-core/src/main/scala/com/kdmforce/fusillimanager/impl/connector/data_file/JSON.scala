package com.kdmforce.fusillimanager.impl.connector.data_file

import akka.stream.scaladsl.{Flow, Source, Framing, JsonFraming}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Entity
import com.kdmforce.fusillimanager.impl.connector.DataFile
import play.api.libs.json.{JsValue, Json, JsNull}
import org.slf4j.{Logger, LoggerFactory}


object JSON extends DataFile[ByteString, String] {

  private final val log: Logger = LoggerFactory.getLogger("JSON")

  override def formatInput(entity: Entity) = {
    log.info("START METHOD FORMATINPUT")

    val outputMap = newMapJsonByteToMap(entity) _

    log.info("START JSON PARSING")
    JsonFraming.objectScanner(Int.MaxValue)
    .map(outputMap)

  }

  override def formatOutput(entity: Entity) = {
    log.info("START METHOD FORMATOUTPUT")
    val header = entity.attributes.map(_.attr_name)

    log.info("START JSON PARSING")
    Flow[Map[String, String]]
      .map(mapToJsonString _)

  }

  def mapToJsonString (doc : Map[String, Any]) : ByteString ={
    var output = "{"
    var bool = 1
    for ((key,value)<-doc){
      if (bool ==1){bool =0 }
      else {output = output + ", "}
      output = output +"\"" + key + "\" : \"" + value +"\""

    }
    ByteString (output + "}")
  }

  def newMapJsonByteToMap (entity: Entity) (doc:ByteString) : Map[String,String] = {
    var outMap = Map[String,String]()
    var jDoc : JsValue = Json.parse(doc.utf8String)

    for (keyholder<-entity.attributes){
      outMap += (keyholder.attr_name -> recursiveKeySearch(keyholder.attr_name,jDoc))
    }
    outMap
  }

  def recursiveKeySearch (key: String, value: JsValue) : String = {
    val splitKey = key.split ("""\.""",2)

    if(splitKey.length <= 1) {
      if ((value\key).get == JsNull) ""
      else (value\key).get.as[String]
    }
    else recursiveKeySearch(splitKey(1), (value\splitKey(0)).get.as[JsValue])

  }
}