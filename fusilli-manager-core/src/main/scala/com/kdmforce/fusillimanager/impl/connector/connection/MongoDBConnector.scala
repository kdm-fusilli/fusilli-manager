package com.kdmforce.fusillimanager.impl.connector.connection

import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.slf4j.{Logger, LoggerFactory}
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import akka.NotUsed
import akka.stream.IOResult
import org.slf4j.{Logger, LoggerFactory}


import scala.concurrent.Await
import scala.concurrent._
import org.mongodb.scala._
import org.mongodb.scala.MongoClient
import org.mongodb.scala.bson.codecs.Macros._
import play.api.libs.json.{JsValue, Json}
import scala.concurrent.duration._
import org.mongodb.scala.model.Filters._






object MongoDBConnector extends Connector[Any]{

  private final val log: Logger = LoggerFactory.getLogger("MONGO")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    val mongoSettings = spawnConnectionScala(repo)
    log.info(s"START CONNECTION ${mongoSettings.toString}")

    val result = findElem(mongoSettings, repo, entity)
    log.info(s"FIND ELEMENT ${result.toString}")

    //mongoSettings.close()
    log.info("END CONNECTION")

    Source(result)
      .map(ByteString(_))


  }

  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")

    val mongoSettings = spawnConnectionScala(repo)
    log.info(s"START CONNECTION ${mongoSettings.toString}")

    val jsonString = createElem(repo, entity, mongoSettings) _
    log.info(s"CREATE ELEMENT ${jsonString.toString}")

    //mongoSettings.close()
    log.info("END CONNECTION")

    Sink.foreach(elem => jsonString(elem.asInstanceOf[ByteString]))
  }


  def spawnConnectionScala(repo: Repo): MongoClient = {

    val uri: String = s"mongodb://${repo.repo_user}:${repo.repo_password}@${repo.host}:${repo.port}/${repo.schema_path}"
    MongoClient(uri)
  }

  def findElem(mongoSettings: MongoClient, repo: Repo, entity: Entity) = {

    val db: MongoDatabase = mongoSettings.getDatabase(repo.schema_path)

    val collection = db.getCollection(entity.entity_name)

    val resultQuery = collection.find().map(dbo => dbo.toJson)

    Await.result(resultQuery.toFuture(), Duration.Inf)
  }

  def createElem(repo: Repo, entity: Entity, mongoSettings: MongoClient)(thing: ByteString) = {

    val jsonString = thing.utf8String

    val doc: Document = Document(jsonString)

    val db: MongoDatabase = mongoSettings.getDatabase(repo.schema_path)

    val collection = db.getCollection(entity.entity_name)

    val query = collection.insertOne(doc)
    Await.result(query.toFuture(), Duration.Inf)
  }

}
