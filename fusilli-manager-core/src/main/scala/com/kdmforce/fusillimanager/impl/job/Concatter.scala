package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{ConfigValue, Job}
import com.kdmforce.fusillimanager.impl.MessageSerializer._

object Concatter {
  def props = Props[Concatter]
}

class Concatter(instance: Int, job: Job, follower: ActorRef ) extends Actor {
  var index = 0

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {

    case a: (SupervisorStop,ActorRef) => 
      a._2 ! SupervisedAck

    case el: Map[String, String] =>
      val result = concatFun(el, job.job_configs)
      index = index + 1
      log.info("CONCATTER OF " + s"${index}" + " ELEMENT")
      //logger ! (LogMessage(instance, JobMessage("CONCATTER RESULT: " + s"${result}", "", job.job_configuration_id)))
      follower ! result
  }

  def concatFun(doc: Map [String,String], jobConfigs: Seq[ConfigValue]): Map[String,String] = {
    var newDoc = doc
    for (j <- jobConfigs) {
      var newVal = List[String]()
      for (attr <- j.input_attrs) {
        newVal = newVal :+ doc(attr)
        newDoc = newDoc - attr
      }
      newDoc = newDoc + (j.output_attrs(0) -> newVal.mkString((j.config \ "spacing").as[String]))

    }
    newDoc
  }
}
