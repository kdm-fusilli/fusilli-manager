package com.kdmforce.fusillimanager.impl.supervisor

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, Router }
import scala.util.Random
import akka.actor.{Actor, ActorRef, PoisonPill, Props, Terminated}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Repo, Job, Pipeline, PipelineStop, Entity}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, LogMessage, PipelineStart, PipelineEnd}
import com.kdmforce.fusillimanager.impl.job._
import com.kdmforce.fusillimanager.impl.job.join_connections.BigSpawner
import play.api.libs.json.{Format, Json, JsValue,JsArray, JsObject}
import com.kdmforce.fusillimanager.impl.job.job_specific_objects.MatcherItems
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, Router }
import com.kdmforce.fusillimanager.impl.connector.ConnectorOut

import scala.util.Random
import com.kdmforce.fusillimanager.impl.MessageSerializer._

object GenericOutputSupervisor {
  def props = Props[GenericOutputSupervisor]
}

class GenericOutputSupervisor(pipeId: Int, repo: Repo, entity: Entity) extends Actor {
  var instances: Int = entity.entity_instances
  if (instances < 2) {
    instances = 1
  }

  val routees = Vector.fill(instances) {
    val r = context.system.actorOf(Props(new ConnectorOut(pipeId, repo, entity)))
    context.watch(r)
    ActorRefRoutee(r)
  }

  val router = Router(RoundRobinRoutingLogic(), routees)

  var index = 0

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case PipelineStart =>
      log.info("START " + (repo.repo_name).toUpperCase())
      logger ! (LogMessage(pipeId, JobMessage("START", "", repo.job_configuration_id)))

    case PipelineEnd =>
      log.info("END " + (repo.repo_name).toUpperCase())
      logger ! (LogMessage(pipeId, JobMessage("END", "", repo.job_configuration_id)))
      self ! PoisonPill

    case PipelineError(ex) =>
      log.error(ex, "ERROR ON " + (repo.repo_name).toUpperCase())
      logger ! (LogMessage(pipeId, JobMessage("ERROR", ex.getMessage, repo.job_configuration_id)))
      self ! PoisonPill

    case el: Map[String, String] =>
      router.route(el, sender())

  }
}

