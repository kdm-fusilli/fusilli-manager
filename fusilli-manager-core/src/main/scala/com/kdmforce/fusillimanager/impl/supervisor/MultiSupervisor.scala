package com.kdmforce.fusillimanager.impl.supervisor

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, BroadcastRoutingLogic, Router }
import scala.util.Random
import com.kdmforce.fusillimanager.impl.job._


object MultiSupervisor {
  def props = Props[MultiSupervisor]
}

class MultiSupervisor(instance: Int, job: Job, predecessors:Int, followers: Seq[ActorRef]) extends Actor {
  var instances: Int = job.job_instances
  if (instances < 2) {
    instances = 1
  }

  val routees = Vector.fill(instances) {
    job.job_name match {
      case "id" =>
        val r = context.system.actorOf(Props(classOf[Mockingbird], instance, job, followers))
        context.watch(r)
        ActorRefRoutee(r)
    }
  }
  val router = Router(RoundRobinRoutingLogic(), routees)
  val killerRouter = Router(BroadcastRoutingLogic(), routees)

  var index = 0
  var prevs = predecessors

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case PipelineStart =>
      log.info("START " + (job.job_name).toUpperCase())
      logger ! (LogMessage(instance, JobMessage("START", "", job.job_configuration_id)))
      for (follower <- followers) { follower ! PipelineStart }

    case PipelineEnd =>
      prevs -= 1
      if (prevs == 0){
          log.info("\nSTOP PROCEDURE OF " + (job.job_name).toUpperCase() + "'S SUPERVISOR INITIALIZED")
          var aliveSons = instances
          killerRouter.route((SupervisorStop, self), sender())
          context.become({
                case SupervisedAck =>
                          aliveSons-=1
                          if(aliveSons ==0){
                                      logger ! (LogMessage(instance, JobMessage("END", "", job.job_configuration_id)))
                                      for (follower <- followers) { follower ! PipelineEnd }
                                      log.info("\nSTOP PROCEDURE OF " + (job.job_name).toUpperCase() + "'S SUPERVISOR SUCCESSFULLY CONCLUDED! CONGRATS!")
                                      self ! PoisonPill
                                  }
                          //else log.info(aliveSons + " more Ack needed")
                case _ => log.error("Unexpected message received at " + job.job_name +"'s supervisor during stop routine")
          })   
      }
      else  log.info((job.job_name).toUpperCase() + " RECEIVED A STOP," + prevs + " MORE NEEDED")

    case PipelineError(ex) =>
      log.error(ex, "ERROR ON " + (job.job_name).toUpperCase())
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, job.job_configuration_id)))
      for (follower <- followers){ follower ! PipelineError(ex) }
      self ! PoisonPill

    case el: Map[String, String] =>
      router.route(el, sender())

  }
}

