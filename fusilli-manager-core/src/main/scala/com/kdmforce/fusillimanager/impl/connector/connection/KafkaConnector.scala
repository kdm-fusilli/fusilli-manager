package com.kdmforce.fusillimanager.impl.connector.connection


import java.util

import org.apache.kafka.clients.consumer.KafkaConsumer
import java.util.{Properties, Collections}

import scala.collection.JavaConverters._
import akka.actor.ActorSystem
import akka.kafka.ConsumerSettings.createKafkaConsumer
import akka.kafka.ProducerSettings.createKafkaProducer
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, ProducerSettings, Subscriptions}
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsValue, Json}
import org.apache.kafka.clients.consumer.KafkaConsumer
import scala.util.control.Breaks._
import scala.concurrent.Future
import akka.Done





object KafkaConnector extends Connector[Any]{

  private final val log: Logger = LoggerFactory.getLogger("KAFKA")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    val kafkaConsumerSettings: ConsumerSettings[String, String] = spawnConsumerConnection(repo)
    log.info(s"START CONNECTION")

    implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

    Consumer
      .committableSource(kafkaConsumerSettings, Subscriptions.topics(entity.entity_name))
      .map { msg =>
        ByteString(msg.record.value)
      }
  }


  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")

    val kafkaProducerSettings = spawnProducerConnection(repo)
    log.info(s"START CONNECTION ${kafkaProducerSettings.toString}")

    val jsonString = createRecordKafkaProducer(entity, kafkaProducerSettings) _
    log.info(s"RECORD PRODUCER ${jsonString.toString}")

    Sink.foreach(elem => jsonString(elem.asInstanceOf[ByteString]))

  }

  def spawnConsumerConnection(repo: Repo): ConsumerSettings[String, String] = {
    implicit val system: ActorSystem = ActorSystem()

    ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
      .withBootstrapServers(s"${repo.host}:${repo.port}")
      .withGroupId(repo.schema_path)
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
      .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
      .withProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "5000")
  }

  def spawnProducerConnection(repo: Repo): ProducerSettings[String, String] = {
    implicit val system: ActorSystem = ActorSystem()

    val producerConfig = system.settings.config.getConfig("akka.kafka.producer")

    ProducerSettings(producerConfig, new StringSerializer, new StringSerializer)
      .withBootstrapServers(s"${repo.host}:${repo.port}")
  }

  def createRecordKafkaProducer(entity: Entity, kafkaProducerSettings: ProducerSettings[String, String])(thing: ByteString) = {
    val jsonString = thing.utf8String
    val record = new ProducerRecord[String, String](entity.entity_name, jsonString)
    val kafkaProducer: KafkaProducer[String, String] = createKafkaProducer(kafkaProducerSettings)

    kafkaProducer.send(record)

    kafkaProducer.close()

  }

}
