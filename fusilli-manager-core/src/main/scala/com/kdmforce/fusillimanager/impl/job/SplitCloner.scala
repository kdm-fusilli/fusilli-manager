package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._

object SplitCloner {
  def props = Props[SplitCloner]
}

class SplitCloner(instance: Int, job: Job, follower: ActorRef ) extends Actor {
  var index = 0

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case a: (SupervisorStop,ActorRef) => 
      a._2 ! SupervisedAck

    case el: Map[String, String] =>
      index = index + 1
      log.info("SPLITCLONER OF " + s"${index}" + " ELEMENT")
      //logger ! (LogMessage(instance, JobMessage("SPLITCLONER RESULT: " + s"${el}", "", job.job_configuration_id)))
      for(word<-el(job.job_configs(0).input_attrs(0)).split((job.job_configs(0).config \ "spacing").as[String])){
        follower ! el - job.job_configs(0).input_attrs(0) + (job.job_configs(0).output_attrs(0) -> word)
      }

  }

}
