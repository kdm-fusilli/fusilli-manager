package com.kdmforce.fusillimanager.impl.job.join_connections

import akka.stream.alpakka.slick.scaladsl.{Slick, SlickSession}
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Repo
import slick.dbio.DBIO
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.{GetResult, JdbcBackend}
import slick.util.AsyncExecutor
import org.mongodb.scala.MongoClient
import org.slf4j.{Logger, LoggerFactory}


object BigSpawner{

  private final val log: Logger = LoggerFactory.getLogger("BIG SPAWNER")
  log.info("START")

	def spawnConnection(repo : Repo) : Any ={

		repo.repo_type match {
      case "mongo" =>
        log.info("START CONNECTION MONGO")
        spawnMongoCollection(repo)

      case "postgresql" =>
        log.info("START CONNECTION POSTGRESQL")
        spawnPostgresConnection(repo)
    }
	}

	def spawnPostgresConnection(repoData : Repo) : JdbcBackend.DatabaseDef = {
    val dbAndSchema = repoData.schema_path.split("/")
    var db = ""
    if(dbAndSchema.size == 2) {
      db = dbAndSchema(0)
    } else {
      db = repoData.schema_path
    }

    Database.forURL(
      s"jdbc:postgresql://${repoData.host}:${repoData.port}/${db}",
      repoData.repo_user,
      repoData.repo_password,
      driver = "org.postgresql.Driver",
      executor = AsyncExecutor.default("PostgresExecutor", 20))
  }

  def spawnMongoCollection(repo : Repo): MongoClient ={
    //MongoClient(s"mongodb://${repoData.repo_user}:${repoData.repo_password}@${repoData.host}:${repoData.port}/${repoData.schema_path}")
    val uri: String = s"mongodb://${repo.repo_user}:${repo.repo_password}@${repo.host}:${repo.port}/${repo.schema_path}?authSource=admin&w=majority&readPreference=primary&ssl=false"
    MongoClient(uri)
  }

}