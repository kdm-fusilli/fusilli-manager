package com.kdmforce.fusillimanager.impl.job.job_specific_objects

import play.api.libs.json.{Format, Json, JsValue,JsObject,JsArray}
import scala.util.Random

object MatcherItems{
/////
def PredicateParser(jsx : JsValue) : Condition ={
    //println("\n\n\n\nSto parsando " + s"${js}" + "\n\n\n\n")
    val js = jsx.as[JsObject]
    if (js.keys.contains("cond_type")){
        val tipo = (js \ "cond_type").get.as[String]
        //println("\n\n\n\nVorrei " + s"${tipo}" + "\n\n\n\n")
        tipo match{
            case "string" => 
      //          println("\n\n\n\n Trovato!\n\n\n\n")
                js.as[BaseStringCon]
            case "number" =>
                js.as[BaseNumericCon]

        }   
    }
    else{

        val condSeq = (js \ "apply_to").get.as[JsArray].value

        new LogicCon((js\"fun").get.as[String],(condSeq.map(PredicateParser(_))).toSeq)
    }

}

//////Common interface
trait Condition{
	def logicalValue(doc: Map[String,String]): Boolean

}


object BaseStringCon{
    implicit val format: Format[BaseStringCon] = Json.format[BaseStringCon]
  }
///////Base case for strings
case class BaseStringCon (verify: String, key: String, value: String, cond_type: String) extends Condition{


	 def logicalValue(doc: Map[String,String]): Boolean ={
        
    	verify match {
    		case "equals" => doc(this.key) == this.value
            case "contains" => doc(this.key).asInstanceOf[String].contains(this.value)
            case "f_contains" => (doc(this.key).asInstanceOf[String]).contains(doc(this.value).toString)
            case "random" => Random.nextInt(2) == 1

    	}

    } 

}

object BaseNumericCon{
    implicit val format: Format[BaseNumericCon] = Json.format[BaseNumericCon]
  }

case class BaseNumericCon (verify: String, first_term: String, second_term: String, cond_type: String) extends Condition{


     def logicalValue(doc: Map[String,String]): Boolean ={
        
        verify match {
            case "equals" => BigDecimal(doc(this.first_term)).equals(BigDecimal(this.second_term))

            case "less_than" => BigDecimal(doc(this.first_term))<(BigDecimal(this.second_term))
            case "greater_than" => BigDecimal(doc(this.first_term))>(BigDecimal(this.second_term))
            case "less_or_equal_than" => BigDecimal(doc(this.first_term))<=(BigDecimal(this.second_term))
            case "greater_or_equal_than" => BigDecimal(doc(this.first_term))>=(BigDecimal(this.second_term))

            case "f_equals" => BigDecimal(doc(this.first_term)).equals(BigDecimal(doc(this.second_term)) )
            case "f_less_than" => BigDecimal(doc(this.first_term))<(BigDecimal(doc(this.second_term)) )
            case "f_greater_than" => BigDecimal(doc(this.first_term))>(BigDecimal(doc(this.second_term)) )
            case "f_less_or_equal_than" => BigDecimal(doc(this.first_term))<=(BigDecimal(doc(this.second_term)) )
            case "f_greater_or_equal_than" => BigDecimal(doc(this.first_term))>=(BigDecimal(doc(this.second_term)) )

        }

    } 

}




//////Inductive case for predicates
case class LogicCon (fun: String, apply_to: Seq[Condition]) extends Condition{

	 def logicalValue(doc: Map[String,String]): Boolean ={
    	fun match {
    		case "AND" => 
    			var boolOut = true
    			for(c<-apply_to){
    				if (c.logicalValue(doc) == false) boolOut = false
    			}
    			boolOut
    		case  "OR" =>
    			var boolOut = false
    			for(c<-apply_to){
    				if (c.logicalValue(doc) == true) boolOut = true
    			}
    			boolOut
    		case "NAND" => 
    			var boolOut = true
    			for(c<-apply_to){
    				if (c.logicalValue(doc) == false) boolOut = false
    			}
    			!boolOut
    		case  "NOR" =>
    			var boolOut = false
    			for(c<-apply_to){
    				if (c.logicalValue(doc) == true) boolOut = true
    			}
    			!boolOut
            case  "X-OR" =>
                var boolOut = false
                for(c<-apply_to){
                    if (c.logicalValue(doc) == true) boolOut = !boolOut
                }
                boolOut
            case  "X-NOR" =>
                var boolOut = false
                for(c<-apply_to){
                    if (c.logicalValue(doc) == true) boolOut = !boolOut
                }
                !boolOut
    	}

    } 


}


/*object LogicCon{
    implicit val format: Format[LogicCon] = Json.format[LogicCon]
  }*/
//////



////
}