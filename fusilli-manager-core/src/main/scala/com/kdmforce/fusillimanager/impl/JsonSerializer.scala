package com.kdmforce.fusillimanager.impl

import com.lightbend.lagom.scaladsl.playjson.JsonSerializer
import com.lightbend.lagom.scaladsl.playjson.JsonSerializerRegistry

import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Pipeline

object MyRegistry extends JsonSerializerRegistry {
  override val serializers = Vector(
    JsonSerializer[Pipeline],
  )
}
