package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import com.kdmforce.fusillimanager.impl.job.job_specific_objects.MatcherItems._

object Matcher {
  def props = Props[Matcher]
}

class Matcher(instance: Int, job: Job,  matchingBranch: ActorRef, unmatchingBranch : ActorRef, proposition : Condition ) extends Actor {
  var index = 0

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case a: (SupervisorStop,ActorRef) => 
      a._2 ! SupervisedAck

    case el: Map[String, String] =>
      index = index + 1
      log.info("MATCHER OF " + s"${index}" + " ELEMENT")
      //logger ! (LogMessage(instance, JobMessage("MATCHER RESULT: " + s"${el}", "", job.job_configuration_id)))
      if (proposition.logicalValue(el)) {
        matchingBranch ! el}
      else {
        unmatchingBranch ! el
      }
  }

}
