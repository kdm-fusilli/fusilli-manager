package com.kdmforce.fusillimanager.impl.connector.connection

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path, FSDataOutputStream}

import akka.stream.IOResult
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import akka.stream.alpakka.hdfs.scaladsl.{HdfsFlow, HdfsSource}
import org.apache.hadoop.fs._
import akka.stream.alpakka.hdfs._
import akka.stream.scaladsl.{Sink, Source}
import java.io.PrintWriter
import org.slf4j.{Logger, LoggerFactory}

import java.io.{File, FileOutputStream, BufferedWriter, OutputStreamWriter}


import scala.concurrent.Future


object HdfsConnector extends Connector[ByteString]{

  private final val log: Logger = LoggerFactory.getLogger("HDFS")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")
    val file = repo.schema_path + entity.entity_name

    val hdfsPath = new Path(file)

    log.info(s"FILE CONNECTION ${file}")
    HdfsSource.data(fileSystemConfiguration(repo, entity), hdfsPath)

  }

  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")

    val hdfsConstructor = insertElem(entity, repo) _
    log.info(s"INSERT ELEMENT")

    Sink.foreach(elem => hdfsConstructor(elem.asInstanceOf[ByteString]))
  }

  def fileSystemConfiguration(repo: Repo, entity: Entity): FileSystem = {

    val conf = new Configuration()
    conf.set(entity.entity_name, s"hdfs://${repo.host}:${repo.port}")
    conf.set("dfs.support.append", "true")

    FileSystem.get(conf)
  }


  //PROPRIO CORRETTO
  def insertElem(entity: Entity, repo: Repo)(thing: ByteString) = {

    val csvString = thing.utf8String
    val fs = fileSystemConfiguration(repo, entity)
    val hdfsPath = new Path(s"${repo.schema_path + entity.entity_name}")
    var output: FSDataOutputStream = null

    if(fs.exists(hdfsPath).equals(false)) {
      output = fs.create(hdfsPath)
    }

    val writer = new PrintWriter(new FileOutputStream(
      new File(hdfsPath.toString),true))
    try {
      writer.write(csvString)
    }
    finally {
      writer.close()
    }
  }

}

