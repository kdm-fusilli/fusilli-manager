package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import com.kdmforce.fusillimanager.impl.job.job_specific_objects.StackerItems._

object Stacker {
  def props = Props[Stacker]
}

class Stacker(instance: Int, job: Job, follower: ActorRef ) extends Actor {
  var index = 0
  val config : StackerConfig = job.job_configs(0).config.as[StackerConfig]

  var counterOf = Map[List[String], Map[String, BigDecimal]]()
 
 
  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case PipelineStart =>
      log.info("START STACKER")
      logger ! (LogMessage(instance, JobMessage("START", "", job.job_configuration_id)))
      follower ! PipelineStart 

    case PipelineEnd =>
      //log.info("STACKER" + instance + " UNSTACKING")

      for((k,v)<-counterOf) follower ! composeOutput( v, k , config.group_me.map(_.out_name))

      log.info("END STACKER")
      logger ! (LogMessage(instance, JobMessage("END", "", job.job_configuration_id)))
      follower ! PipelineEnd 
      self ! PoisonPill


    case PipelineError(ex) =>
      log.error(ex, "ERROR ON STACKER")
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, job.job_configuration_id)))
      follower ! PipelineError(ex)
      self ! PoisonPill
 
    case el: Map[String, String] =>
      index = index + 1
      log.info("STACKER OF " + s"${index}" + " ELEMENT")

      stackFun(config, el)
  }




  def composeOutput(mapToNums: Map[ String, BigDecimal], listOfValues : List[String], listOfKeys:  List[String]) : Map[String, String] = {
    var outMap : Map[String, String] = ( for(i<-0 until listOfValues.length ) yield (listOfKeys(i)-> listOfValues(i))).toMap
    
    for((k,v)<-mapToNums) outMap = outMap + ( k->v.toString )

    outMap
  }

  def stackFun(config: StackerConfig, el: Map[String, String]) = {
    val matchList = for (key <- config.group_me) yield el(key.key_name)

    if (counterOf.contains(matchList))
      counterOf = counterOf + (matchList ->  (  for (key <- config.sum_me)
        yield ( key.out_name ->
          ( counterOf(matchList)(key.out_name) + BigDecimal(el(key.key_name)) )  ) ).toMap)
    else counterOf = counterOf + (matchList -> ( for (key <- config.sum_me)
      yield ( key.out_name ->
        BigDecimal(el(key.key_name)) ) ).toMap )
  }


}
