package com.kdmforce.fusillimanager.api

import play.api.libs.json.{Format, Json, JsValue}

object FusilliEndpointSerializer {
  case class Policy(
    entity_id: Int,
    attrs: Seq[PolicyAttribute]
  )

  object Policy {
    implicit val format: Format[Policy] = Json.format[Policy]
  }

  case class Attribute(
    attr_name:String,
    attr_type: String,
    format: String,
    length: String
  )
  object Attribute{
    implicit val format: Format[Attribute] = Json.format[Attribute]
  }

  case class PolicyAttribute(
    attr_name: String,
    policy_behaviour: String,
    parameter : String
  )
  object PolicyAttribute{
    implicit val format: Format[PolicyAttribute] = Json.format[PolicyAttribute]
  }

  case class Entity(
    entity_id: Int,
    entity_name: String,
    entity_type: String,
    separator: String,
    attributes: Seq[Attribute],
    connection_edge: Int,
    entity_instances: Int
  )
  object Entity{
    implicit val format: Format[Entity] = Json.format[Entity]
  }

  case class Repo(
    repo_name:String,
    repo_user:String,
    repo_password: String,
    repo_type: String,
    job_configuration_id: Int,
    host: String,
    port: Int,
    schema_path: String,
    delay: Int,
    entities: Seq[Entity]
  )
  object Repo{
    implicit val format: Format[Repo] = Json.format[Repo]
  }
  case class ConfigValue(
    input_attrs: Seq[String],
    output_attrs: Seq[String],
    config: JsValue
  )
  object ConfigValue{
    implicit val format: Format[ConfigValue] = Json.format[ConfigValue]
  }

  case class Job(
    job_configuration_id: Int,
    job_name: String,
    input_edges: Seq[Int],
    output_edges: Seq[Int],
    job_instances: Int,
    job_configs: Seq[ConfigValue]
  )
  object Job{
    implicit val format: Format[Job] = Json.format[Job]
  }

  case class Pipeline(
    instance_id: Int,
    pipeline_id: Int,
    policies_id: String,
    version: String,
    logger: String,
    policies: Seq[Policy],
    inputs:Seq[Repo],
    outputs:Seq[Repo],
    jobs:Seq[Job]
  )

  object Pipeline{
    implicit val format: Format[Pipeline] = Json.format[Pipeline]
  }

  case class PipelineStop(
      pipeline_id: Int
  )
  object PipelineStop{
    implicit val format: Format[PipelineStop] = Json.format[PipelineStop]
  }
}
