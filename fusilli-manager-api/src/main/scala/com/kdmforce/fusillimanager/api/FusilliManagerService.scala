package com.kdmforce.fusillimanager.api

import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Pipeline
import com.kdmforce.fusillimanager.api.DiscoveryEndpointSerializer.DiscoveryDoc
import com.kdmforce.fusillimanager.api.DiscoveryEndpointSerializer.DiscoRepoOut


import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api._
import com.lightbend.lagom.scaladsl.api.transport._

/**
 * The Fusilli Manager service interface.
 * <p>
 * This describes everything that Lagom needs to know about how to serve and
 * consume the FusilliManagerService.
 */
trait FusilliManagerService extends Service {

  def pipelineStart(): ServiceCall[Pipeline, Done]
  def discoveryStart(): ServiceCall[DiscoveryDoc, Done]
  def pipelineStop (): ServiceCall[Pipeline, Done]
  def postme(): ServiceCall[String, Done]

  override final def descriptor: Descriptor = {
    import Service._
    named("fusilli-manager")
      .withCalls(
        restCall(Method.POST, "/start/", pipelineStart _),
        restCall(Method.POST,"/autodiscovery/", discoveryStart _),
        restCall(Method.POST,"/postme/", postme _),
        restCall(Method.POST,"/stop/", pipelineStop _)
      )
      .withAutoAcl(true)
  }
}
