
import com.lightbend.lagom.core.LagomVersion
organization in ThisBuild := "com.kdmforce"
version in ThisBuild := "0.8.3"
//logLevel := Level.Error


// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.13.2"

def dockerSettings = Seq(
  dockerBaseImage := "adoptopenjdk/openjdk8",
  dockerRepository := sys.props.get("docker.registry")
)



val macwire = "com.softwaremill.macwire" %% "macros" % "2.3.3" % "provided"
val scalaTest = "org.scalatest" %% "scalatest" % "3.1.1" % Test

val postgresjdbc = "org.postgresql" % "postgresql" % "42.2.11"
val mysql = "mysql" % "mysql-connector-java" % "8.0.20"
val jacksoncsv = "com.fasterxml.jackson.dataformat" % "jackson-dataformat-csv" % "2.10.3"
val jsch = "com.jcraft" % "jsch" % "0.1.55"
val scalajhttp =  "org.scalaj" %% "scalaj-http" % "2.4.2"
val hasher = "com.outr" %% "hasher" % "1.2.2"
val oraclejdbc = "com.oracle.ojdbc" % "ojdbc8" % "19.3.0.0"

val akkaclustertools = "com.typesafe.akka" %% "akka-cluster-tools" % LagomVersion.akka
val akkaclustertyped = "com.typesafe.akka" %% "akka-cluster-typed" % LagomVersion.akka

val akkacluster = "com.typesafe.akka" %% "akka-cluster" % LagomVersion.akka
val akkadiscovery = "com.typesafe.akka" %% "akka-discovery" % LagomVersion.akka

val clusterbootstrap = "com.lightbend.akka.management" %% "akka-management-cluster-bootstrap" % "1.0.8"

val ftpVersion =   "com.lightbend.akka" %% "akka-stream-alpakka-ftp" % "2.0.1"
val ftpAkkaStream =  "com.typesafe.akka" %% "akka-stream" % LagomVersion.akka

val slick = "com.lightbend.akka" %% "akka-stream-alpakka-slick" % "2.0.1"
val PostgreSQL = "org.postgresql" % "postgresql" % "42.2.14"
val OracleDB = "com.oracle.database.jdbc" % "ojdbc10" % "19.6.0.0"
val MySQL = "mysql" % "mysql-connector-java" % "8.0.20"
val Kafka = "com.typesafe.akka" %% "akka-stream-kafka" % "2.0.4"

val akkaHttpSprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.8"

val logbackKafkaAppender = "com.github.danielwegener" % "logback-kafka-appender" % "0.2.0-RC2"
val logbackClassic = "ch.qos.logback" % "logback-classic" % "1.2.3"

val alpakkafile = "com.lightbend.akka" %% "akka-stream-alpakka-file" % "2.0.1"
val csv = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "2.0.1"

val kubernetesapi = "com.lightbend.akka.discovery" %% "akka-discovery-kubernetes-api" % "1.0.3"

val jsonParse = "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"
val jsonstreaming = "com.lightbend.akka" %% "akka-stream-alpakka-json-streaming" % "2.0.2"

val sprayJson = "io.spray" %%  "spray-json" % "1.3.5"

val hdfs = "com.lightbend.akka" %% "akka-stream-alpakka-hdfs" % "2.0.2"
val hadoopclient = "org.apache.hadoop" % "hadoop-client" % "3.3.0"
val scalalibrary = "org.scala-lang" % "scala-library" % "2.13.3"
val catscore = "org.typelevel" %% "cats-core" % "2.2.0"
val hadoophdfs = "org.apache.hadoop" % "hadoop-hdfs" % "3.3.0"

val mongodb = "com.lightbend.akka" %% "akka-stream-alpakka-mongodb" % "2.0.2"
val mongodriver = "org.mongodb.scala" %% "mongo-scala-driver" % "4.1.1"
val mongodbdriver = "org.mongodb" % "mongodb-driver-reactivestreams" % "4.1.0"
val mongodbdriverasync = "org.mongodb" % "mongodb-driver-async" % "3.12.7"

val alpakkaxml = "com.lightbend.akka" %% "akka-stream-alpakka-xml" % "2.0.2"

val alpakkaamqp = "com.lightbend.akka" %% "akka-stream-alpakka-amqp" % "2.0.2"




lazy val `fusilli-manager` = (project in file("."))
  .aggregate(`fusilli-manager-api`, `fusilli-manager-core`)

lazy val `fusilli-manager-api` = (project in file("fusilli-manager-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `fusilli-manager-core` = (project in file("fusilli-manager-core"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslTestKit,
      akkacluster,
      akkaclustertyped,
      macwire,
      postgresjdbc,
      jacksoncsv,
      jsch,
      scalajhttp,
      hasher,
      scalaTest,
      oraclejdbc,
      clusterbootstrap,
      akkaclustertools,
      akkadiscovery,
      lagomScaladslCluster,
      kubernetesapi,
      ftpVersion,
      ftpAkkaStream,
      slick,
      PostgreSQL,
      OracleDB,
      MySQL,
      alpakkafile,
      csv,
      Kafka,
      akkaHttpSprayJson,
      logbackKafkaAppender,
      logbackClassic,
      sprayJson,
      hdfs,
      hadoopclient,
      scalalibrary,
      catscore,
      hadoophdfs,
      mongodb,
      mongodriver,
      mongodbdriver,
      mongodbdriverasync,
      jsonstreaming,
      alpakkaxml,
      alpakkaamqp
    )
  )
  .settings(lagomForkedTestSettings)
  .settings(dockerSettings)
  .dependsOn(`fusilli-manager-api`)

lagomCassandraEnabled in ThisBuild := false
lagomKafkaEnabled in ThisBuild := false

javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)
